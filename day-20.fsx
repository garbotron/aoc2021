let (algorithm, inputImage) =
  let txt = System.IO.File.ReadAllText "input/day-20.txt"
  let split = txt.Trim().Split "\n\n"
  let algo = split.[0] |> Seq.map ((=) '#') |> Seq.toList
  let image =
    split.[1].Split '\n'
    |> Seq.indexed
    |> Seq.collect (fun (y, s) -> s |> Seq.indexed |> Seq.map (fun (x, c) -> (x, y), c = '#'))
    |> Map
  algo, image

let neighbors (x, y) = seq {
  for y in { y - 1 .. y + 1 } do
    for x in { x - 1 .. x + 1 } do yield (x, y) }

let enhance defaultPixel image =
  let coords = image |> Map.keys
  let minX = (coords |> Seq.map fst |> Seq.min) - 1
  let minY = (coords |> Seq.map snd |> Seq.min) - 1
  let maxX = (coords |> Seq.map fst |> Seq.max) + 1
  let maxY = (coords |> Seq.map snd |> Seq.max) + 1

  let binary pixel =
    pixel
    |> neighbors
    |> Seq.rev
    |> Seq.indexed
    |> Seq.filter (fun (_, p) -> image |> Map.tryFind p |> Option.defaultValue defaultPixel)
    |> Seq.map (fun (i, _) -> 1 <<< i)
    |> Seq.sum

  Seq.allPairs { minX .. maxX } { minY .. maxY }
  |> Seq.map (fun p -> p, algorithm.[binary p])
  |> Map

// Based on the algorithm rules for [0b] and [111111111b], distant space may blink back and forth from lit to unlit.
let rec repeatEnhance count defaultPixel image =
  let nextDefaultPixel = if defaultPixel then 0x1FF else 0
  match count with
  | 0 -> image
  | n -> image |> enhance defaultPixel |> repeatEnhance (n - 1) algorithm.[nextDefaultPixel]

let litPixelCount image =
  image |> Map.toSeq |> Seq.filter snd |> Seq.length

printfn "Part 1: %d" (inputImage |> repeatEnhance 2 false |> litPixelCount)
printfn "Part 2: %d" (inputImage |> repeatEnhance 50 false |> litPixelCount)
