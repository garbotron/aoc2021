// We will model the state as a map of (timer value -> fish count).
let input =
  let str = System.IO.File.ReadAllText "input/day-07.txt"
  str.Split ','
  |> Seq.map int
  |> Seq.toList

let leastFuelRequired fuelCalc =
  Seq.initInfinite (fun x -> input |> List.sumBy (fun y -> fuelCalc x y))
  |> Seq.pairwise
  |> Seq.skipWhile (fun (x, y) -> x > y)
  |> Seq.head
  |> fst

let fuelP1 a b =
  abs (a - b)

let fuelP2 a b =
  // Sum of all numbers 0..x = (x(x + 1))/2
  let x = abs (a - b)
  (x * (x + 1)) / 2

printfn "Part 1: %d" (leastFuelRequired fuelP1)
printfn "Part 2: %d" (leastFuelRequired fuelP2)
