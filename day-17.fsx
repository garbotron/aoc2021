(*
We'll assume the lower bound for initial x velocity is vx = 1.
An upper bound for initial x velocity is vx = xMax + 1.
This will cause us to completely skip the target horizontally after the first step.

A lower bound for initial y velocity is vy = yMin - 1 (downward).
This will cause us to completely skip the target vertically after the first step.
A simple upper bound for initial y velocity is vy = -yMin (upward).
On the way down, when y=0, vy will be -(initial vy + 1). In this case, vy = yMin - 1.

Now that we have these ranges, we simulate each shot and see if it hits.
There are a lot of optimizations that could be done, but they aren't necessary.
*)

open System.Text.RegularExpressions

let input = System.IO.File.ReadAllText "input/day-17.txt"
let inputMatch = Regex.Match (input, @"x=(.+)\.\.(.+), y=(.+)\.\.(.+)")
let xMin = inputMatch.Groups.[1].Value |> int
let xMax = inputMatch.Groups.[2].Value |> int
let yMin = inputMatch.Groups.[3].Value |> int
let yMax = inputMatch.Groups.[4].Value |> int

type State = { X: int; Y: int; VX: int; VY: int }
type Result = Miss | Hit of State

let rec simulate (xMin, xMax) (yMin, yMax) state =
  let state = { X = state.X + state.VX
                Y = state.Y + state.VY
                VX = max 0 (state.VX - 1)
                VY = state.VY - 1 }
  if state.X > xMax || state.Y < yMin then Miss
  elif state.X >= xMin && state.Y <= yMax then Hit state
  else simulate (xMin, xMax) (yMin, yMax) state

// Part 1: simulate each VY alone with VX=0. Height will be sum(1..max VY) = (max VY * (max VY + 1)) / 2.
{ 1 .. -yMin }
|> Seq.map (fun vy -> { X = 0; Y = 0; VX = 0; VY = vy })
|> Seq.filter (fun s -> s |> simulate (0, 0) (yMin, yMax) <> Miss)
|> Seq.last
|> fun s -> printfn "Part 1: %d" ((s.VY * (s.VY + 1)) / 2)

// Part 2: simulate every combination.
Seq.allPairs { 1 .. xMax + 1 } { yMin - 1 .. -yMin }
|> Seq.map (fun (vx, vy) -> { X = 0; Y = 0; VX = vx; VY = vy })
|> Seq.map (simulate (xMin, xMax) (yMin, yMax))
|> Seq.filter ((<>) Miss)
|> Seq.length
|> printfn "Part 2: %d"
