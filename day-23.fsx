let infinity = 1000000

let parseState lines =
  let char2pod = function
    | '.' -> Some 0
    | 'A' -> Some 1
    | 'B' -> Some 2
    | 'C' -> Some 3
    | 'D' -> Some 4
    | _ -> None
  lines
  |> Seq.indexed
  |> Seq.collect (fun (y, line) -> line |> Seq.indexed |> Seq.map (fun (x, c) -> (x, y), char2pod c))
  |> Seq.filter (snd >> Option.isSome)
  |> Seq.map (fun (a, b) -> a, Option.get b)
  |> Map

let part1state =
  System.IO.File.ReadAllLines "input/day-23.txt"
  |> parseState

let part2state =
  let lines = System.IO.File.ReadAllLines "input/day-23.txt" |> Array.toList
  let lines = lines.[0..2] @ ["  #D#C#B#A#"; "  #D#B#A#C#"] @ lines.[3..]
  lines |> parseState

let targetState state =
  state
  |> Map.map (fun (x, y) _ -> if y = 1 then 0 else (x - 1) / 2)

let neighbors (x, y) state =
  seq { x - 1, y; x + 1, y; x, y - 1; x, y + 1; }
  |> Seq.filter (fun a -> state |> Map.containsKey a)

let dist (x, y) (x', y') =
  abs (x - x') + abs (y - y')

let possibleMoves (x, y) state =
  let rec hallwayStops x dx =
    if x = 3 || x = 5 || x = 7 || x = 9 then
      hallwayStops (x + dx) dx
    elif state |> Map.tryFind (x, 1) = Some 0 then
      (x, 1) :: hallwayStops (x + dx) dx
    else
      []

  let pod = state |> Map.find (x, y)
  let targetX = pod * 2 + 1
  if y = 1 then
    // We're in the hallway. We can only move into the target room, and only if it contains only the correct pods.
    // There will never be a reason to move less than all the way in.
    let hallwayXs = if x < targetX then { x + 1 .. targetX } else { x - 1 .. -1 .. targetX }
    if hallwayXs |> Seq.exists (fun x -> state |> Map.find (x, y) <> 0) then
      Seq.empty
    else
      let roomYs = { y + 1 .. infinity } |> Seq.takeWhile (fun y -> state |> Map.containsKey (targetX, y))
      let fillY = roomYs |> Seq.rev |> Seq.skipWhile (fun y -> state |> Map.find (targetX, y) = pod) |> Seq.head
      if { fillY .. -1 .. 2 } |> Seq.forall (fun y -> state |> Map.find (targetX, y) = 0) then
        seq { targetX, fillY }
      else
        Seq.empty
  elif y > 2 && { y - 1 .. -1 .. 2 } |> Seq.exists (fun y -> state |> Map.find (x, y) <> 0) then
    // We're in a room, but there's another pod in front of us. We can't move.
    Seq.empty
  elif x = targetX then
    if { y + 1 .. infinity }
        |> Seq.takeWhile (fun y -> state |> Map.containsKey (x, y))
        |> Seq.forall (fun y -> state |> Map.find (x, y) = pod) then
      // We're in the target room and it's filled up from the back with the correct pods. We will never move again.
      Seq.empty
    else
      // We're in the target room but there's also someone else here. We can move into the hallway.
      hallwayStops (x - 1) -1 @ hallwayStops (x + 1) 1
  else
    // We're in some room other than the target room. We can move into the hallway.
    hallwayStops (x - 1) -1 @ hallwayStops (x + 1) 1

let allPossibleMoves state =
  state
  |> Map.toSeq
  |> Seq.filter (snd >> (<>) 0)
  |> Seq.collect (fun (p, _) -> state |> possibleMoves p |> Seq.map (fun q -> p, q))

let solve state =
  let mutable cache = Map.empty
  let mutable best = infinity
  let stack = System.Collections.Generic.Stack<int * Map<int * int, int>>()
  let targetState = state |> targetState

  stack.Push(0, state)
  while stack.Count > 0 do
    let (cur, state) = stack.Pop()
    if cur >= best then ()
    elif state = targetState then
      best <- cur
    else
      match cache |> Map.tryFind state with
      | Some x when x <= cur -> ()
      | _ ->
        cache <- cache |> Map.add state cur
        for (p, q) in allPossibleMoves state do
          let pod = state |> Map.find p
          let stepCost = pown 10 (pod - 1)
          let cost = dist p q * stepCost
          let updatedState = state |> Map.add p 0 |> Map.add q pod
          stack.Push(cur + cost, updatedState)

  best

printfn "Part 1: %d" (part1state |> solve)
printfn "Part 2: %d" (part2state |> solve)
