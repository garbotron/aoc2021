type Player = { Position: int; Score: int64 }
type State = { P1: Player; P2: Player; P1Turn: bool; DieValue: int; RollCount: int }

let initialState =
  let players =
    System.IO.File.ReadAllLines "input/day-21.txt"
    |> Array.map (fun s -> { Position = int (s.Split ':').[1]; Score = 0 })
  { P1 = players.[0]; P2 = players.[1]; P1Turn = true; DieValue = 1; RollCount = 0 }

let wrap1based max num =
  ((num - 1) % max) + 1

let mapInc key map =
  let cur = map |> Map.tryFind key |> Option.defaultValue 0L
  map |> Map.add key (cur + 1L)

let simRoll rollValue state =
  let player = if state.P1Turn then state.P1 else state.P2
  let pos = player.Position + rollValue |> wrap1based 10
  let player = { Position = pos; Score = player.Score + int64 pos }
  { state with
      P1 = if state.P1Turn then player else state.P1
      P2 = if state.P1Turn then state.P2 else player
      P1Turn = not state.P1Turn }

let rec part1 state =
  let roll state =
    state.DieValue, {
      state with
        DieValue = state.DieValue + 1 |> wrap1based 100
        RollCount = state.RollCount + 1 }

  let turn state =
    let (a, state) = roll state
    let (b, state) = roll state
    let (c, state) = roll state
    state |> simRoll (a + b + c)

  if state.P1.Score >= 1000 then
    int64 state.RollCount * state.P2.Score
  elif state.P2.Score >= 1000 then
    int64 state.RollCount * state.P1.Score
  else
    state |> turn |> part1

let part2 initialState =
  // For part 2, we keep a cache of the number of wins for P1 and P2 based on state.
  // DieValue and RollCount aren't used (those are part 1 only).
  let mutable cache = Map.empty

  let allRolls = seq {
    for a in 1 .. 3 do
      for b in 1 .. 3 do
        for c in 1 .. 3 do
          yield a + b + c }

  let rec getWinCount state =
    match cache |> Map.tryFind state with
    | Some x -> x
    | None ->
      let scores =
        if state.P1.Score >= 21 then (1L, 0L)
        elif state.P2.Score >= 21 then (0L, 1L)
        else
          allRolls
          |> Seq.map (fun r -> state |> simRoll r |> getWinCount)
          |> Seq.reduce (fun (a, b) (c, d) -> a + c, b + d)
      cache <- cache |> Map.add state scores
      scores

  let (p1, p2) = getWinCount initialState
  max p1 p2

printfn "Part 1: %d" (initialState |> part1)
printfn "Part 2: %d" (initialState |> part2)
