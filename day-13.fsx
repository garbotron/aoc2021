type Fold = FoldX of int | FoldY of int
type State = {
  Dots: Set<int * int>
  Folds: Fold list
}

let parseDot (str: string) =
  let split = str.Split ','
  (int split.[0]), (int split.[1])

let parseFold (str: string) =
  let split = str.Split '='
  if split.[0].EndsWith "x" then
    FoldX (int split.[1])
  else
    FoldY (int split.[1])

let parseState (str: string) =
  let split = str.Trim().Split "\n\n"
  let dots = split.[0].Split "\n"
  let folds = split.[1].Split "\n"
  let dots =
    dots
    |> Seq.fold (fun dots x -> dots |> Set.add (parseDot x)) Set.empty
  let folds =
    folds
    |> Array.toList
    |> List.map parseFold
  { Dots = dots; Folds = folds }

let input =
  System.IO.File.ReadAllText "input/day-13.txt"
  |> parseState

let fold dots fold =
  match fold with
  | FoldX fx ->
    dots |> Set.map (fun (x, y) -> (if x < fx then x else x - (2 * (x - fx))), y)
  | FoldY fy ->
    dots |> Set.map (fun (x, y) -> x, (if y < fy then y else y - (2 * (y - fy))))

printfn "Part 1: %d" (input.Folds.[0] |> fold input.Dots |> Seq.length)

printfn "Part 2:"
let result = input.Folds |> Seq.fold fold input.Dots
let maxX = result |> Seq.map fst |> Seq.max
let maxY = result |> Seq.map snd |> Seq.max
for y in 0 .. maxY do
  for x in 0 .. maxX do
    printf (if result |> Set.contains (x, y) then "#" else " ")
  printfn ""
