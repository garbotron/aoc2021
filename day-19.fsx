(*
First, add all of the beacons from sensor 1 to the known set (default rotation).
Then repeat:
  Find the furthest-out known beacon in each direction (+/- x/y/z).
  For each of these, find the nearest other known beacon to form a pair.
  Note the delta(x, y, z) of this pair.
  For each unknown scanner, check if a pair of becons has the same delta (accounting for rotation).
  If so, try to match the rest of the beacons to the known set after transforming for rotation.
  If there are at least 12 matches, add the whole thing to the known set and go back to the top.
*)

type Point = int * int * int
type Transform = Point -> Point
type State = {
  Uncorrelated: Set<Set<Point>>
  Correlated: Set<Point> list
  Beacons: Set<Point>
  Scanners: Point list }

let parseCoord (str: string) =
  let split = str.Split ','
  (int split.[0], int split.[1], int split.[2])

let parseScanner (str: string) =
  str.Split '\n'
  |> Seq.skip 1
  |> Seq.map parseCoord
  |> Set

let input =
  let str = System.IO.File.ReadAllText "input/day-19.txt"
  str.Trim().Split "\n\n"
  |> Seq.map parseScanner
  |> Seq.toList

let add (x, y, z) (dx, dy, dz) =
  x + dx, y + dy, z + dz

let dist (x1, y1, z1) (x2, y2, z2) =
  x2 - x1, y2 - y1, z2 - z1

let manhattan (a, b) =
  let (x, y, z) = dist a b
  abs x + abs y + abs z

let rotationTransforms = seq {
  // Adapted from https://stackoverflow.com/questions/16452383/how-to-get-all-24-rotations-of-a-3-dimensional-array.
  let roll (x, y, z) = (x, z, -y)
  let turn (x, y, z) = (-y, x, z)
  let mutable v = id
  for _ in { 1 .. 2 } do
    for _ in { 1 .. 3 } do
      v <- v >> roll
      yield v
      for _ in { 1 .. 3 } do
        v <- v >> turn
        yield v
    v <- v >> roll >> turn >> roll }

let findAllBeacons scanners =
  // Get all pairs that we will try to match on for a particular correlated scanner.
  let matchPairs scanner =
    // I haven't been able to come up with any way to limit this search.
    // So we'll just do every single pair of beacons.
    Seq.allPairs scanner scanner

  // For a given scanner + match pair + rotation transform, if there are leat 12 points that correlate to the
  // known set, return a new state with the scanner merged into the known set. Else return None.
  let correlate known (matchA, matchB) scanner state rotation =
    // For all points in the scanner that also have a point at (p+delta), establish a baseline offset with the
    // known set, and try to correlate all of the points in the scanner. Return whichever result has the most matches.
    let delta = dist matchA matchB
    scanner
    |> Seq.filter (fun p -> scanner |> Set.contains (p |> add (rotation delta)))
    |> Seq.fold (fun state p ->
      let transform = dist p >> rotation >> add matchA
      let matches = scanner |> Set.map transform |> Set.intersect known
      if Set.count matches < 12 then state
      else
        { state with
            Uncorrelated = state.Uncorrelated |> Set.remove scanner
            Correlated = (scanner |> Set.map transform) :: state.Correlated
            Scanners = transform (0, 0, 0) :: state.Scanners }) state

  // For a given scanner + match pair, try all possible rotations to see if they correlate with at least 12 beacons.
  // For each match, perform the correlation and update the state as needed.
  let correlateAllRotations known matchPair state scanner =
    rotationTransforms
    |> Seq.fold (correlate known matchPair scanner) state

  // For the given match pair and set of known points, find all unsolved scanners for which there are at least 12
  // points that correlate. Perform a full correlation on each of these scanners and update the state as needed.
  let correlateAllUnsolved known state matchPair =
    state.Uncorrelated
    |> Seq.fold (correlateAllRotations known matchPair) state

  let rec loop state =
    match state.Correlated with
    | [] -> state
    | head :: tail ->
      let state = { state with Correlated = tail; Beacons = Set.union state.Beacons head }
      matchPairs head
      |> Seq.fold (correlateAllUnsolved head) state
      |> loop

  match scanners with
  | [] -> failwith "no scanners"
  | head :: tail ->
    loop {
      Correlated = [head]
      Uncorrelated = Set tail
      Beacons = Set.empty
      Scanners = List.empty }

let result = findAllBeacons input
printfn "Part 1: %d" result.Beacons.Count
printfn "Part 2: %d" (Seq.allPairs result.Scanners result.Scanners |> Seq.map manhattan |> Seq.max)
