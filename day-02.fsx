type State = { Depth: int; Position: int; Aim: int }
let defaultState = { Depth = 0; Position = 0; Aim = 0 }

type Command =
  | Forward of int
  | Up of int
  | Down of int

let parseCommand (str: string) =
  let words = str.Split ' '
  let v = int words.[1]
  match words.[0] with
  | "forward" -> Forward v
  | "up" -> Up v
  | "down" -> Down v
  | _ -> failwith "bad command"

let input =
  System.IO.File.ReadAllLines "input/day-02.txt"
  |> Seq.map parseCommand

let performP1 state = function
  | Up x -> { state with Depth = state.Depth - x }
  | Down x -> { state with Depth = state.Depth + x }
  | Forward x -> { state with Position = state.Position + x }

let performP2 state = function
  | Up x -> { state with Aim = state.Aim - x }
  | Down x -> { state with Aim = state.Aim + x }
  | Forward x -> { state with Position = state.Position + x; Depth = state.Depth + state.Aim * x }

let p1 = input |> Seq.fold performP1 defaultState
printfn "Part 1: %d" (p1.Depth * p1.Position)

let p2 = input |> Seq.fold performP2 defaultState
printfn "Part 2: %d" (p2.Depth * p2.Position)
