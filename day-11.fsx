type State = {
  Map: Map<int * int, int>
  FlashCount: int }

let input =
  let foldLine map (y, line) =
    line
    |> Seq.indexed
    |> Seq.fold (fun map (x, c) -> map |> Map.add (x, y) (c |> string |> int)) map
  System.IO.File.ReadAllLines "input/day-11.txt"
  |> Seq.indexed
  |> Seq.fold foldLine Map.empty
  |> fun m -> { Map = m; FlashCount = 0 }

let neighbors (x, y) =
  Seq.allPairs { x - 1 .. x + 1 } { y - 1 .. y + 1 }
  |> Seq.except [x, y]
  |> Seq.filter (fun (x, y) -> x >= 0 && x <= 9 && y >= 0 && y <= 9)

let rec flash state =
  match state.Map |> Map.toSeq |> Seq.tryFind (fun (_, x) -> x > 9) with
  | None -> state // nothing flashes
  | Some (coord, _) ->
    // Reset the flashed coordinate to a super negative value, so it doesn't go off again.
    let map = state.Map |> Map.add coord System.Int32.MinValue
    let map =
      coord
      |> neighbors
      |> Seq.fold (fun state x -> state |> Map.add x (state.[x] + 1)) map
    flash { Map = map; FlashCount = state.FlashCount + 1 }

let step state =
  let add1 s = { s with Map = s.Map |> Map.map (fun _ x -> x + 1) }
  let clear s = { s with Map = s.Map |> Map.map (fun _ x -> max x 0) }
  state |> add1 |> flash |> clear

let steps num =
  { 1 .. num } |> Seq.map (fun _ -> step) |> Seq.reduce (>>)

let p1 = input |> steps 100
printfn "Part 1: %d" p1.FlashCount

// For part 2, we will unfold an infinite sequence of steps and look for when the flash count increases by 100.
Seq.unfold (fun state -> Some (state, step state)) input
|> Seq.indexed
|> Seq.pairwise
|> Seq.find (fun ((_, s1), (_, s2)) -> s2.FlashCount - s1.FlashCount = 100)
|> fun (_, (i, _)) -> printfn "Part 2: %d" i
