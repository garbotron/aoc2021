// We will model the state as a map of (timer value -> fish count).
let initialState =
  let str = System.IO.File.ReadAllText "input/day-06.txt"
  str.Split ','
  |> Seq.map int
  |> Seq.groupBy id
  |> Seq.map (fun (i, v) -> i, Seq.length v |> int64)
  |> Map

let mapAdd key num map =
  let cur = map |> Map.tryFind key |> Option.defaultValue 0L
  map |> Map.add key (cur + num)

let passDay state =
  let folder nextMap timer fish =
    match timer with
    | 0 -> nextMap |> mapAdd 6 fish |> mapAdd 8 fish
    | n -> nextMap |> mapAdd (n - 1) fish
  state |> Map.fold folder Map.empty

let passDays n =
  { 1 .. n } |> Seq.map (fun _ -> passDay) |> Seq.reduce (>>)

initialState
|> passDays 80
|> Map.toSeq
|> Seq.sumBy snd
|> printfn "Part 1: %d"

initialState
|> passDays 256
|> Map.toSeq
|> Seq.sumBy snd
|> printfn "Part 2: %d"
