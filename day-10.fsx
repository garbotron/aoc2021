let input =
  System.IO.File.ReadAllLines "input/day-10.txt"
  |> Seq.map Seq.toList

type ParseResult =
  | Success of char list
  | Incomplete of char list
  | IllegalChar of char

let rec parseChunk (terminator: char option) (chars: char list) =
  let continueParsing terminator result =
    match result, terminator with
    | Success lst, _ -> lst |> parseChunk terminator
    | IllegalChar c, _ -> IllegalChar c
    | Incomplete lst, None -> Incomplete lst
    | Incomplete lst, Some x -> Incomplete (x :: lst)
  match chars, terminator with
  | [], None -> Success []
  | [], Some c -> Incomplete [c]
  | c :: rest, Some t when t = c -> Success rest
  | '(' :: rest, t -> rest |> parseChunk (Some ')') |> continueParsing t
  | '[' :: rest, t -> rest |> parseChunk (Some ']') |> continueParsing t
  | '{' :: rest, t -> rest |> parseChunk (Some '}') |> continueParsing t
  | '<' :: rest, t -> rest |> parseChunk (Some '>') |> continueParsing t
  | c :: _, _ -> IllegalChar c

let p1score = function
  | IllegalChar ')' -> 3
  | IllegalChar ']' -> 57
  | IllegalChar '}' -> 1197
  | IllegalChar '>' -> 25137
  | _ -> 0

input
|> Seq.map (parseChunk None >> p1score)
|> Seq.sum
|> printfn "Part 1: %d"

let rec p2score cur = function
  | [] -> cur
  | ')' :: rest -> rest |> p2score (cur * 5L + 1L)
  | ']' :: rest -> rest |> p2score (cur * 5L + 2L)
  | '}' :: rest -> rest |> p2score (cur * 5L + 3L)
  | '>' :: rest -> rest |> p2score (cur * 5L + 4L)
  | _ -> failwith "bad closing char"

input
|> Seq.map (parseChunk None)
|> Seq.choose (function | Incomplete x -> Some x | _ -> None)
|> Seq.map (List.rev >> p2score 0L)
|> Seq.sort
|> Seq.toList
|> fun x -> x.[x.Length / 2]
|> printfn "Part 2: %d"
