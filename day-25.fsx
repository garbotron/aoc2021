type State = {
  Width: int
  Height: int
  Eastward: Set<int * int>
  Southward: Set<int * int> }

let initialState =
  let lines = System.IO.File.ReadAllLines "input/day-25.txt"
  let map ch =
    lines
    |> Seq.indexed
    |> Seq.collect (fun (y, s) -> s |> Seq.indexed |> Seq.map (fun (x, c) -> (x, y), c))
    |> Seq.filter (snd >> (=) ch)
    |> Seq.map fst
    |> Set
  { Width = lines.[0].Length
    Height = lines.Length
    Eastward = map '>'
    Southward = map 'v' }

let east (x, y) state =
  if x = state.Width - 1 then (0, y) else (x + 1, y)

let south (x, y) state =
  if y = state.Height - 1 then (x, 0) else (x, y + 1)

let occupied coord state =
  state.Eastward |> Set.contains coord || state.Southward |> Set.contains coord

let moveEast state =
  let move a = if state |> occupied (east a state) then a else east a state
  { state with Eastward = state.Eastward |> Set.map move }

let moveSouth state =
  let move a = if state |> occupied (south a state) then a else south a state
  { state with Southward = state.Southward |> Set.map move }

let step = moveEast >> moveSouth

Seq.unfold (fun s -> Some (s, step s)) initialState
|> Seq.indexed
|> Seq.pairwise
|> Seq.skipWhile (fun ((_, a), (_, b)) -> a <> b)
|> Seq.head
|> snd
|> fst
|> printfn "Part 1: %d"
