let input =
  System.IO.File.ReadAllLines "input/day-03.txt"
  |> Seq.map (fun s -> System.Convert.ToInt32(s, 2))
  |> Seq.toList

let isOne pos num = (num &&& (1 <<< pos)) <> 0
let oneCount pos nums = nums |> List.filter (isOne pos) |> List.length

let gamma =
  let mostCommonBit pos =
    if oneCount pos input > input.Length / 2 then 1 else 0
  { 0 .. 11 }
  |> Seq.map (fun i -> mostCommonBit i <<< i)
  |> Seq.reduce (|||)

let epsilon = 0xFFF &&& ~~~gamma

printfn "Part 1: %d" (gamma * epsilon)

let rec oxygenGeneratorRating pos = function
  | [x] -> x
  | nums ->
    let ones = nums |> oneCount pos
    let zeroes = nums.Length - ones
    let filter = if ones >= zeroes then (isOne pos) else (not << isOne pos)
    nums |> List.filter filter |> oxygenGeneratorRating (pos - 1)

let rec co2ScrubberRating pos = function
  | [x] -> x
  | nums ->
    let ones = nums |> oneCount pos
    let zeroes = nums.Length - ones
    let filter = if zeroes <= ones then (not << isOne pos) else (isOne pos)
    nums |> List.filter filter |> co2ScrubberRating (pos - 1)

printfn "Part 2: %d" (oxygenGeneratorRating 11 input * co2ScrubberRating 11 input)
