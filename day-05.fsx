open System.Text.RegularExpressions

let input =
  let parseLine (str: string) =
    let m = Regex.Match(str, @"(\d+),(\d+) -> (\d+),(\d+)")
    let num (i: int) = int m.Groups.[i].Value
    (num 1, num 2), (num 3, num 4)
  System.IO.File.ReadAllLines "input/day-05.txt"
  |> Array.toList
  |> List.map parseLine

let hvLines lines =
  lines |> List.filter (fun ((ax, ay), (bx, by)) -> ax = bx || ay = by)

let rec points ((ax, ay), (bx, by)) =
  let step a b =
    if a = b then 0 elif a < b then 1 else -1
  let dx = step ax bx
  let dy = step ay by
  List.ofSeq <| seq {
    let mutable x, y = ax, ay
    yield x, y
    while (x, y) <> (bx, by) do
      x <- x + dx
      y <- y + dy
      yield x, y }

let overlapCount lines =
  lines
  |> List.collect points
  |> List.groupBy id
  |> List.filter (fun (_, lst) -> lst.Length > 1)
  |> List.length

printfn "Part 1: %d" (input |> hvLines |> overlapCount)
printfn "Part 2: %d" (input |> overlapCount)
