// This is pretty damn slow (~30 min) but this is the 3rd full rewrite and I can't think of a better way.

type Axes = {
  Values: int array
  IndexOf: Map<int, int> }

type State = {
  XAxes: Axes
  YAxes: Axes
  ZAxes: Axes
  On: Set<int * int * int> }

let fullInput =
  let parseLine (str: string) =
    let nums = System.Text.RegularExpressions.Regex.Matches(str, @"-?\d+")
    let num i = int nums.[i].Value
    (str.StartsWith "on", (num 0, num 2, num 4), (num 1, num 3, num 5))
  System.IO.File.ReadAllLines "input/day-22.txt"
  |> Seq.map parseLine
  |> Seq.toList

let initAreaInput =
  fullInput |> List.takeWhile (fun (_, (x, _, _), _) -> x >= -50)

let defaultState input =
  let axes values =
    let values = values |> Seq.toArray |> Array.sort
    { Values = values; IndexOf = values |> Seq.indexed |> Seq.map (fun (a, b) -> b, a) |> Map }
  { XAxes = input |> Seq.collect (fun (_, (a, _, _), (b, _, _)) -> [a; b + 1]) |> axes
    YAxes = input |> Seq.collect (fun (_, (_, a, _), (_, b, _)) -> [a; b + 1]) |> axes
    ZAxes = input |> Seq.collect (fun (_, (_, _, a), (_, _, b)) -> [a; b + 1]) |> axes
    On = Set.empty }

let setCube state (on, (ax, ay, az), (bx, by, bz)) =
  printfn "set cube: %A" (on, (ax, ay, az), (bx, by, bz))
  let axisRange lower upper axis = seq {
    let mutable idx = axis.IndexOf |> Map.find lower
    while axis.Values.[idx] <= upper do
      yield axis.Values.[idx]
      idx <- idx + 1 }

  let coords = Set.ofSeq <| seq {
    for x in state.XAxes |> axisRange ax bx do
      for y in state.YAxes |> axisRange ay by do
        for z in state.ZAxes |> axisRange az bz do yield (x, y, z) }

  printfn "num coords: %d" (coords |> Seq.length)
  { state with On = if on then Set.union state.On coords else Set.difference state.On coords }

let totalVolume state =
  printfn "total voume..."
  let nextAxis value axes =
    let idx = axes.IndexOf |> Map.find value
    axes.Values.[idx + 1]
  let volume (x, y, z) =
    let dx = (state.XAxes |> nextAxis x) - x
    let dy = (state.YAxes |> nextAxis y) - y
    let dz = (state.ZAxes |> nextAxis z) - z
    int64 dx * int64 dy * int64 dz
  state.On |> Seq.sumBy volume

initAreaInput
|> Seq.fold setCube (defaultState initAreaInput)
|> totalVolume
|> printfn "Part 1: %d"

fullInput
|> Seq.fold setCube (defaultState fullInput)
|> totalVolume
|> printfn "Part 2: %d"
