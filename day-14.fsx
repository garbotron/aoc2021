(*
  For part 2, we obviously can't do each replacement in-line.
  Instead, we keep track of each pair of elements in a map, so we can perform the replacements en masse.
  We also need to keep track of how many of each element there are for the result.
*)

type State = { ElemCount: Map<char, int64>; PairCount: Map<char * char, int64> }

let (polymerTemplate: char list), (rules: Map<char * char, char>) =
  let input = System.IO.File.ReadAllLines "input/day-14.txt"
  (input.[0] |> Seq.toList,
   input
   |> Seq.skip 2
   |> Seq.map (fun s -> (s.[0], s.[1]), s.[6])
   |> Map)

let countMap lst =
  lst |> List.groupBy id |> Map |> Map.map (fun _ x -> x |> List.length |> int64)

let initialState =
  { ElemCount = polymerTemplate |> countMap
    PairCount = polymerTemplate |> List.pairwise |> countMap }

let mapAdd key count map =
  match map |> Map.tryFind key with
  | None -> map |> Map.add key count
  | Some c -> map |> Map.add key (c + count)

let step state =
  let fold state (a, b) count =
    let insertion = rules |> Map.find (a, b)
    { ElemCount = state.ElemCount |> mapAdd insertion count
      PairCount =
        state.PairCount
        |> mapAdd (a, b) -count
        |> mapAdd (a, insertion) count
        |> mapAdd (insertion, b) count }
  state.PairCount |> Map.fold fold state

let steps cnt =
  { 1 .. cnt } |> Seq.map (fun _ -> step) |> Seq.reduce (>>)

let run reps state =
  let state = state |> steps reps
  let min = state.ElemCount |> Map.toSeq |> Seq.map snd |> Seq.min
  let max = state.ElemCount |> Map.toSeq |> Seq.map snd |> Seq.max
  (max - min)

printfn "Part 1: %d" (initialState |> run 10)
printfn "Part 2: %d" (initialState |> run 40)
