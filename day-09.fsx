let input =
  let parseLine (y: int, str: string) =
    str
    |> Seq.indexed
    |> Seq.map (fun (x, c) -> (x, y), c |> string |> int)
  System.IO.File.ReadAllLines "input/day-09.txt"
  |> Seq.indexed
  |> Seq.collect parseLine
  |> Map

let allCoords =
  input |> Map.toSeq |> Seq.map fst

let neighbors (x, y) =
  seq { x + 1, y; x - 1, y; x, y + 1; x, y - 1 }

let rec findTerminalLowPoint coord =
  let isLower neighbor =
    match input |> Map.tryFind neighbor with
    | None -> false
    | Some x -> x < input.[coord]
  match (coord |> neighbors |> Seq.tryFind isLower) with
  | None -> coord // there is no lower neighbor - this is the low point
  | Some n -> findTerminalLowPoint n

let basins =
  allCoords
  |> Seq.filter (fun x -> input.[x] <> 9)
  |> Seq.map (fun x -> findTerminalLowPoint x, x)
  |> Seq.groupBy fst
  |> Seq.map (fun (x, y) -> x, Seq.length y)

basins
|> Seq.map (fun (coord, _) -> input.[coord] + 1)
|> Seq.sum
|> printfn "Part 1: %d"

basins
|> Seq.map snd
|> Seq.sortDescending
|> Seq.take 3
|> Seq.reduce (*)
|> printfn "Part 2: %d"
