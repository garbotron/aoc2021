[<ReferenceEquality>]
type SnailNumContent =
  | Literal of int
  | Pair of SnailNum * SnailNum
and SnailNum =
  { mutable Content: SnailNumContent }

let isLiteral x = match x.Content with Literal _ -> true | _ -> false
let isPair x = match x.Content with Pair _ -> true | _ -> false
let literal x = match x.Content with Literal x -> x | _ -> failwith "not literal"
let pair x = match x.Content with Pair (a, b) -> (a, b) | _ -> failwith "not pair"

let parseSnailNum (str: string) =
  let rec loop chars =
    let skip ch chars =
      if List.head chars <> ch then failwith (sprintf "missing %c" ch)
      chars |> List.tail
    match chars with
    | '[' :: rest ->
      let (a, rest) = loop rest
      let (b, rest) = loop (rest |> skip ',')
      let num = { Content = Pair (a, b) }
      num, rest |> skip ']'
    | x :: rest when x >= '0' && x <= '9' ->
      { Content = Literal (x |> string |> int) }, rest
    | _ -> failwith "bad char"
  loop (str |> Seq.toList) |> fst

let rec snail2str = function
| { Content = Literal x } -> string x
| { Content = Pair (a, b) } -> sprintf "[%s,%s]" (snail2str a) (snail2str b)

type Order = Forward | Backward
let traverse order node =
  let rec loop num level =
    let thisNode = seq { level, num }
    match order, num.Content with
    | Forward, Pair (a, b) -> Seq.concat [loop a (level + 1); thisNode; loop b (level + 1)]
    | Backward, Pair (a, b) -> Seq.concat [loop b (level + 1); thisNode; loop a (level + 1)]
    | _ -> thisNode
  loop node 0

let firstLiteralAfter root node =
  root |> traverse Forward |> Seq.map snd |> Seq.takeWhile ((<>) node) |> Seq.filter isLiteral |> Seq.tryLast

let firstLiteralBefore root node =
  root |> traverse Backward |> Seq.map snd |> Seq.takeWhile ((<>) node) |> Seq.filter isLiteral |> Seq.tryLast

let explode root node =
  let (a, b) = pair node
  match (firstLiteralAfter root a) with
  | None -> ()
  | Some n -> n.Content <- Literal (literal n + literal a)
  match (firstLiteralBefore root b) with
  | None -> ()
  | Some n -> n.Content <- Literal (literal n + literal b)
  node.Content <- Literal 0

let split node =
  let v = literal node
  let a = v / 2
  let b = v - a
  node.Content <- Pair ({ Content = Literal a }, { Content = Literal b })

let rec reduce num =
  match (num |> traverse Forward |> Seq.tryFind (fun (l, n) -> isPair n && l = 4)) with
  | Some (_, node) -> explode num node; reduce num
  | None ->
    match (num |> traverse Forward |> Seq.tryFind (fun (_, n) -> isLiteral n && literal n >= 10)) with
    | Some (_, node) -> split node; reduce num
    | None -> num

let add num1 num2 =
  reduce { Content = Pair (num1, num2) }

let rec magnitude = function
| { Content = Literal x } -> x
| { Content = Pair (a, b) } -> (magnitude a * 3) + (magnitude b * 2)

let inputLines = System.IO.File.ReadAllLines "input/day-18.txt" |> Seq.toList

inputLines
|> List.map parseSnailNum
|> Seq.reduce add
|> magnitude
|> printfn "Part 1: %d"

Seq.allPairs inputLines inputLines
|> Seq.filter (fun (a, b) -> a <> b)
|> Seq.map (fun (a, b) -> add (parseSnailNum a) (parseSnailNum b) |> magnitude)
|> Seq.max
|> printfn "Part 2: %d"
