type Cave = { Name: string; Big: bool; mutable Connections: Cave list }

let inputPairs =
  System.IO.File.ReadAllLines "input/day-12.txt"
  |> Seq.map (fun s -> s.Split '-')

let caves =
  inputPairs
  |> Seq.collect id
  |> Seq.distinct
  |> Seq.toList
  |> List.map (fun c -> { Name = c; Big = System.Char.IsUpper c.[0]; Connections = []})

for pair in inputPairs do
  let a = caves |> List.find (fun x -> x.Name = pair.[0])
  let b = caves |> List.find (fun x -> x.Name = pair.[1])
  a.Connections <- b :: a.Connections
  b.Connections <- a :: b.Connections

let rec pathCountP1 cavesSeen cave =
  if cave.Name = "end" then 1
  else
    cave.Connections
    |> Seq.filter (fun c -> c.Big || (not (cavesSeen |> Set.contains c.Name)))
    |> Seq.sumBy (pathCountP1 (cavesSeen |> Set.add cave.Name))

let rec pathCountP2 cavesSeen repeatDone cave =
  if cave.Name = "end" then 1
  else
    let connections =
      cave.Connections
      |> List.filter (fun c -> c.Big || (not (cavesSeen |> Set.contains c.Name)))
      |> List.sumBy (pathCountP2 (cavesSeen |> Set.add cave.Name) repeatDone)
    let repeats =
      if repeatDone then 0
      else
        cave.Connections
        |> List.filter (fun c -> (not c.Big) && (cavesSeen |> Set.contains c.Name) && c.Name <> "start")
        |> List.sumBy (pathCountP2 (cavesSeen |> Set.add cave.Name) true)
    connections + repeats

let start = caves |> Seq.find (fun x -> x.Name = "start")
printfn "Part 1: %d" (start |> pathCountP1 Set.empty)
printfn "Part 2: %d" (start |> pathCountP2 Set.empty false)
