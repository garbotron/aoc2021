type Operator = Sum | Product | Min | Max | GreaterThan | LessThan | EqualTo

type Packet = {
  Version: int64
  Data: PacketData }

and PacketData =
| LiteralValue of int64
| Operation of Operator * Packet list

let num2bin num =
  let rec loop = function
  | 0L -> []
  | n -> (n % 2L = 1L) :: loop (n >>> 1)
  num |> loop |> List.rev

let bin2num bin =
  let rec loop = function
  | [] -> 0L
  | bit :: rest -> (if bit then 1L else 0L) + (loop rest <<< 1)
  bin |> List.rev |> loop

let rec binPad size = function
| x when List.length x >= size -> x
| x -> binPad size (false :: x)

let hex2bin hexStr =
  let ch2bin ch =
    System.Convert.ToInt64(string ch, 16)
    |> num2bin
    |> binPad 4
  hexStr
  |> Seq.collect ch2bin
  |> Seq.toList

let rec parsePacket bin =
  if bin |> List.length < 6 then
    None
  else
    let (version, bin) = bin |> List.splitAt 3
    let (packetType, bin) = bin |> List.splitAt 3
    let (data, bin) =
      match packetType |> bin2num with
      | 0L -> parseOperator bin Sum
      | 1L -> parseOperator bin Product
      | 2L -> parseOperator bin Min
      | 3L -> parseOperator bin Max
      | 4L -> parseLiteralValue bin
      | 5L -> parseOperator bin GreaterThan
      | 6L -> parseOperator bin LessThan
      | 7L -> parseOperator bin EqualTo
      | _ -> failwith "bad packet type"
    Some ({ Version = version |> bin2num; Data = data }, bin)

and parseLiteralValue bin =
  let rec parseBinNum bin =
    let (chunk, bin) = bin |> List.splitAt 5
    if chunk |> List.head then
      let (rest, bin) = bin |> parseBinNum
      (chunk |> List.tail) @ rest, bin
    else
      (chunk |> List.tail), bin
  let (num, bin) = parseBinNum bin
  (num |> bin2num |> LiteralValue), bin

and parseOperator bin packetType =
  let (lenType, bin) = bin |> List.splitAt 1
  if lenType = [false] then
    let (len, bin) = bin |> List.splitAt 15
    let len = len |> bin2num |> int32
    let (substream, bin) = bin |> List.splitAt len
    Operation (packetType, List.unfold parsePacket substream), bin
  else
    let (count, bin) = bin |> List.splitAt 11
    let count = count |> bin2num
    let (packets, bin) =
      { 1L .. count }
      |> Seq.fold (fun (packets, bin) _ ->
          let (packet, bin) = bin |> parsePacket |> Option.get
          packets @ [packet], bin) ([], bin)
    Operation (packetType, packets), bin

let rec versionSum packet =
  match packet.Data with
  | LiteralValue _ -> packet.Version
  | Operation (_, packets) -> packet.Version + (packets |> List.sumBy versionSum)

let rec eval packet =
  match packet.Data with
  | LiteralValue x             -> x
  | Operation (Sum, p)         -> p |> List.sumBy eval
  | Operation (Product, p)     -> p |> List.map eval |> List.reduce (*)
  | Operation (Min, p)         -> p |> List.map eval |> List.min
  | Operation (Max, p)         -> p |> List.map eval |> List.max
  | Operation (GreaterThan, p) -> if eval p.[0] > eval p.[1] then 1L else 0L
  | Operation (LessThan, p)    -> if eval p.[0] < eval p.[1] then 1L else 0L
  | Operation (EqualTo, p)     -> if eval p.[0] = eval p.[1] then 1L else 0L

let inputStr = System.IO.File.ReadAllText "input/day-16.txt"
let inputPacket = inputStr.Trim() |> hex2bin |> parsePacket |> Option.get |> fst

printfn "Part 1: %d" (inputPacket |> versionSum)
printfn "Part 2: %d" (inputPacket |> eval)
