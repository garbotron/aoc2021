let input = System.IO.File.ReadAllLines "input/day-01.txt" |> Seq.map int

input
|> Seq.pairwise
|> Seq.filter (fun (x, y) -> y > x)
|> Seq.length
|> printfn "Part 1: %d"

input
|> Seq.windowed 3
|> Seq.map Array.sum
|> Seq.pairwise
|> Seq.filter (fun (x, y) -> y > x)
|> Seq.length
|> printfn "Part 2: %d"
