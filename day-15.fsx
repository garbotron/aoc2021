#r "nuget: FSharpx.Collections"
open FSharpx.Collections

let infinity = 99999999
let inputLines = System.IO.File.ReadAllLines "input/day-15.txt"

let smallMapW = inputLines.[0].Length
let smallMapH = inputLines.Length
let smallMap =
  Seq.allPairs { 0 .. smallMapW - 1 } { 0 .. smallMapH - 1 }
  |> Seq.map (fun (x, y) -> (x, y), inputLines.[y].[x] |> string |> int)
  |> Map

let replicateSmallMap bigMap (sx, sy) =
  let roll n = if n > 9 then n - 9 else n
  let fold bigMap (x, y) v =
    bigMap |> Map.add (x + sx * smallMapW, y + sy * smallMapH) (roll (v + sx + sy))
  smallMap |> Map.fold fold bigMap

let bigMapW = smallMapW * 5
let bigMapH = smallMapH * 5
let bigMap =
  Seq.allPairs { 0 .. 4 } { 0 .. 4 }
  |> Seq.fold replicateSmallMap Map.empty

let neighbors (x, y) map =
   seq { x + 1, y; x, y + 1; x - 1, y; x, y - 1 }
   |> Seq.filter (fun cell -> map |> Map.containsKey cell)

type AStarState = {
  OpenSet: IPriorityQueue<int * (int * int)>
  GScore: Map<int * int, int> }

let aStar (map: Map<int * int, int>) (destX: int, destY: int): int =
  let h (x, y) = abs (destX - x) + abs (destY - y)

  let initialState = {
    OpenSet = PriorityQueue.empty false |> PriorityQueue.insert (h (0, 0), (0, 0))
    GScore = Map [(0, 0), 0] }

  let rec explore state =
    if state.OpenSet |> PriorityQueue.isEmpty then
      state
    else
      let ((_, current), priq) = state.OpenSet |> PriorityQueue.pop
      if current = (destX, destY) then state
      else
        map
        |> neighbors current
        |> Seq.fold (exploreNeighbor current) { state with OpenSet = priq }
        |> explore

  and exploreNeighbor current state neighbor =
    let tentativeGScore = (state.GScore |> Map.find current) + (map |> Map.find neighbor)
    if tentativeGScore < (state.GScore |> Map.tryFind neighbor |> Option.defaultValue infinity) then
      { state with
          OpenSet = state.OpenSet |> PriorityQueue.insert (tentativeGScore + h neighbor, neighbor)
          GScore = state.GScore |> Map.add neighbor tentativeGScore }
    else
      state

  let result = initialState |> explore
  result.GScore |> Map.find (destX, destY)

let run partNum map w h =
  let start = System.DateTime.Now
  let result = aStar map (w - 1, h - 1)
  let time = System.DateTime.Now - start
  printfn "Part %d: %d (%A)" partNum result time

run 1 smallMap smallMapW smallMapH
run 2 bigMap bigMapW bigMapH
