(*
The MONAD has 14 input instructions, with a similar block after each.

The block looks like this:
  inp w
  mul x 0
  add x z
  mod x 26
  div z <a>
  add x <b>
  eql x w
  eql x 0
  mul y 0
  add y 25
  mul y x
  add y 1
  mul z y
  mul y 0
  add y w
  add y <c>
  mul y x
  add z y

a, b, and c differ per block

rewriting this as C-ish pseudocode we get this:
func(a, b, c):
  w = input()
  x = 0
  x += z
  x %= 26
  z /= a
  x += b
  x = (x == w)
  x = (x == 0)
  y = 0
  y += 25
  y *= x
  y += 1
  z *= y
  y = 0
  y += w
  y += c
  y *= x
  z += y

Notice that w, x and y are all reset each time. The only persistent variable is z.
We can crunch this a bit more:
func(a, b, c)
  w = input()
  x = ((z % 26) + b != w)
  z = ((z / a) * ((25 * x) + 1)) + ((w + c) * x)

If we rephrase the comparison as an if-statement and do some math, it looks like this:
func(a, b, c)
  w = input()
  if ((z % 26) + b != w)
    z = (26 * (z / a)) + c + w
  else
    z = z / a

a is always 1 or 26. b and c are all over the place (b [-16..15], c [2..16])
Let's take the first (a, b, c) = (1, 15, 13):
  w = input()
  if ((z % 26) + 15 != w)
    z = (26 * z) + 13 + w

Let's think about z as a base-26 number (written a..z).
If so, this first block does this:
  w = input()
  if (last digit of z is not (w - 15))
    z = z with the digit (w + 13) appended

All of the blocks for which a=1 look basically like this.
How about block 5 (26, -11, 6)?
We end up with something like this:
  w = input()
  if (last digit of z is not (w + 11))
    z = replace the last digit with (w + 6)
  else
    z = cut off the last digit

The blocks are:
  0  | 1  15  13 | if LD not (w-15), append (w+13)
  1  | 1  10  16 | if LD not (w-10), append (w+16)
  2  | 1  12  2  | if LD not (w-12), append (w+2)
  3  | 1  10  8  | if LD not (w-10), append (w+8)
  4  | 1  14  11 | if LD not (w-14), append (w+11)
  5  | 26 -11 6  | if LD not (w+11), truncate, else replace with (w+6)
  6  | 1  10  12 | if LD not (w-10), append (w+12)
  7  | 26 -16 2  | if LD not (w+16), truncate, else replace with (w+2)
  8  | 26 -9  2  | if LD not (w+9), truncate, else replace with (w+2)
  9  | 1  11  15 | if LD not (w-11), append (w+15)
  10 | 26 -8  1  | if LD not (w+8), truncate, else replace with (w+1)
  11 | 26 -8  10 | if LD not (w+8), truncate, else replace with (w+10)
  12 | 26 -10 14 | if LD not (w+10), truncate, else replace with (w+14)
  13 | 26 -9  10 | if LD not (w+9), truncate, else replace with (w+10)

Note that all of the appending blocks check for LD != (w-n) where n is at least 10.
This will never be true, since w-n will always be negative. So these appends are ALWAYS performed.

Rephrased:
  0  | 1  15  13 | append (w+13)
  1  | 1  10  16 | append (w+16)
  2  | 1  12  2  | append (w+2)
  3  | 1  10  8  | append (w+8)
  4  | 1  14  11 | append (w+11)
  5  | 26 -11 6  | if LD not (w+11), truncate, else replace with (w+6)
  6  | 1  10  12 | append (w+12)
  7  | 26 -16 2  | if LD not (w+16), truncate, else replace with (w+2)
  8  | 26 -9  2  | if LD not (w+9), truncate, else replace with (w+2)
  9  | 1  11  15 | append (w+15)
  10 | 26 -8  1  | if LD not (w+8), truncate, else replace with (w+1)
  11 | 26 -8  10 | if LD not (w+8), truncate, else replace with (w+10)
  12 | 26 -10 14 | if LD not (w+10), truncate, else replace with (w+14)
  13 | 26 -9  10 | if LD not (w+9), truncate, else replace with (w+10)

Now, we need to end up with 0 at the end. This requires that we truncate all of the digits.
We have 7 blocks that append a digit, so all the others MUST truncate a digit.
So what numbers will do that? We can break up the append/truncates into matching pairs:
  4 <-> 5
  6 <-> 7
  3 <-> 8
  9 <-> 10
  2 <-> 11
  1 <-> 12
  0 <-> 13

In each of these cases, we want to add the largest digit that can be replaced.
For instance, w13 needs to remove the digit (l) added by w0. l=w0+13 and l=w13+9.
The highest legal value for w0 is 5 (which causes w13 to be 9).
Following the same logic:
  w4 = 9 :: w5 = 9
  w6 = 9 :: w7 = 5
  w3 = 9 :: w8 = 8
  w9 = 2 :: w10 = 9
  w2 = 9 :: w11 = 3
  w1 = 3 :: w12 = 9
  w0 = 5 :: w13 = 9

SO! The answer IS!: 53999995829399

For part 2, all the same rules apply. We just take the lowest possible value in each case instead of the highest.
  w4 = 1 :: w5 = 1
  w6 = 5 :: w7 = 1
  w3 = 2 :: w8 = 1
  w9 = 1 :: w10 = 8
  w2 = 7 :: w11 = 1
  w1 = 1 :: w12 = 7
  w0 = 1 :: w13 = 5

Part 2 is: 11721151118175
*)

// The code in this file doesn't solve anything... I just wrote it for debug as I was figuring it out.
type Cpu = {
  Registers: Map<char, int64>
  Input: int64 list }

type Variable =
  | Reg of char
  | Num of int64

type Instruction =
  | Inp of char
  | Add of char * Variable
  | Mul of char * Variable
  | Div of char * Variable
  | Mod of char * Variable
  | Eql of char * Variable

let monad =
  let parseInstruction (str: string) =
    let words = str.Split ' '
    let a = words.[1].[0]
    let b () = if System.Char.IsLetter words.[2].[0] then Reg words.[2].[0] else Num (int64 words.[2])
    match words.[0] with
    | "inp" -> Inp a
    | "add" -> Add (a, b ())
    | "mul" -> Mul (a, b ())
    | "div" -> Div (a, b ())
    | "mod" -> Mod (a, b ())
    | "eql" -> Eql (a, b ())
    | _ -> failwith "bad instruction"
  System.IO.File.ReadAllLines "input/day-24.txt"
  |> Seq.map parseInstruction

let getReg reg cpu =
  cpu.Registers |> Map.tryFind reg |> Option.defaultValue 0

let resolveVariable var cpu =
  match var with
  | Reg x -> cpu |> getReg x
  | Num x -> x

let setReg reg var cpu =
  { cpu with Registers = cpu.Registers |> Map.add reg (cpu |> resolveVariable var) }

let exec cpu instruction =
  match instruction, cpu with
  | Inp r, { Input = head :: tail } -> { cpu with Input = tail } |> setReg r (Num head)
  | Add (r, v), _ -> cpu |> setReg r (Num ((cpu |> getReg r) + (cpu |> resolveVariable v)))
  | Mul (r, v), _ -> cpu |> setReg r (Num ((cpu |> getReg r) * (cpu |> resolveVariable v)))
  | Div (r, v), _ -> cpu |> setReg r (Num ((cpu |> getReg r) / (cpu |> resolveVariable v)))
  | Mod (r, v), _ -> cpu |> setReg r (Num ((cpu |> getReg r) % (cpu |> resolveVariable v)))
  | Eql (r, v), _ -> cpu |> setReg r (Num (if (cpu |> getReg r) = (cpu |> resolveVariable v) then 1 else 0))
  | _ -> failwith "X"

let splitInput num =
  num |> string |> Seq.toList |> List.map (string >> int64)

let runMonad cpu =
  monad |> Seq.fold exec cpu

let simulate cpu =
  let simBlock a b c cpu =
    let w = cpu.Input |> List.head
    let z = cpu |> getReg 'z'
    let x = if (z % 26L) + b <> w then 1L else 0L
    let z = ((z / a) * ((25L * x) + 1L)) + ((w + c) * x)
    { cpu with Input = cpu.Input |> List.tail } |> setReg 'z' (Num z)

  cpu
  |> simBlock 1 15 13
  |> simBlock 1 10 16
  |> simBlock 1 12 2
  |> simBlock 1 10 8
  |> simBlock 1 14 11
  |> simBlock 26 -11 6
  |> simBlock 1 10 12
  |> simBlock 26 -16 2
  |> simBlock 26 -9 2
  |> simBlock 1 11 15
  |> simBlock 26 -8 1
  |> simBlock 26 -8 10
  |> simBlock 26 -10 14
  |> simBlock 26 -9 10

let base26 x =
  let rec loop x =
    if x < 26L then
      'a' + (char x) |> string
    else
      loop (x / 26L) + loop (x % 26L)
  loop x
