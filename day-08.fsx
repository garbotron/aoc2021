type Entry = { Patterns: Set<char> list; Output: Set<char> list }

let parseEntry (line: string) =
  let split = line.Split " | "
  { Patterns = split.[0].Split ' ' |> Array.toList |> List.map Set
    Output = split.[1].Split ' ' |> Array.toList |> List.map Set }

let input =
  System.IO.File.ReadAllLines "input/day-08.txt"
  |> Seq.map parseEntry

input
|> Seq.collect (fun x -> x.Output)
|> Seq.filter (fun s -> [2; 3; 4; 7] |> List.contains s.Count)
|> Seq.length
|> printfn "Part 1: %d"

let decode entry =
  let scrambled1 = entry.Patterns |> List.find (fun x -> x.Count = 2)
  let scrambled7 = entry.Patterns |> List.find (fun x -> x.Count = 3)
  let scrambled4 = entry.Patterns |> List.find (fun x -> x.Count = 4)
  let scrambled8 = entry.Patterns |> List.find (fun x -> x.Count = 7)
  let segCounts =
    [ 'a' .. 'g' ]
    |> List.map (fun x -> x, entry.Patterns |> List.filter (Set.contains x) |> List.length)

  // First, we can figure out 'a' as the only element in '7' that isn't in '1'.
  let a = Set.difference scrambled7 scrambled1 |> Seq.head

  // We can figure out 'e' as the only segment present in only 4 different digits.
  let e = segCounts |> List.find (snd >> (=) 4) |> fst

  // Similarly, 'b' is present in 6 segments and 'f' is present in 9 segments. All other digits are not unique.
  let b = segCounts |> List.find (snd >> (=) 6) |> fst
  let f = segCounts |> List.find (snd >> (=) 9) |> fst

  // We can deduce 'c' as the other signal in '1' that isn't 'f'.
  let c = scrambled1 |> Seq.find (fun x -> x <> f)

  // We can figure out 'd' as the remaining unknown segment in '4'.
  let d =  Set.difference scrambled4 (Set [b; c; f]) |> Seq.head

  // We can figure out 'g' as the remaining unknown segment in '8'.
  let g = Set.difference scrambled8 (Set [a; b; c; d; e; f]) |> Seq.head

  // Now we can create decode the proper output signals.
  let map = Map [a, 'a'; b, 'b'; c, 'c'; d, 'd'; e, 'e'; f, 'f'; g, 'g']
  let decodeLetter set = set |> Seq.map (fun x -> map |> Map.find x) |> Set
  entry.Output |> List.map decodeLetter

let signalToDigit (signal: Set<char>) =
  match (signal |> Seq.sort |> Seq.toList) with
  | [ 'a'; 'b'; 'c'; 'e'; 'f'; 'g' ] -> 0
  | [ 'c'; 'f' ] -> 1
  | [ 'a'; 'c'; 'd'; 'e'; 'g' ] -> 2
  | [ 'a'; 'c'; 'd'; 'f'; 'g' ] -> 3
  | [ 'b'; 'c'; 'd'; 'f' ] -> 4
  | [ 'a'; 'b'; 'd'; 'f'; 'g' ] -> 5
  | [ 'a'; 'b'; 'd'; 'e'; 'f'; 'g' ] -> 6
  | [ 'a'; 'c'; 'f' ] -> 7
  | [ 'a'; 'b'; 'c'; 'd'; 'e'; 'f'; 'g' ] -> 8
  | [ 'a'; 'b'; 'c'; 'd'; 'f'; 'g' ] -> 9
  | _ -> failwith "Unknown digit"

let signalsToNumber (signals: Set<char> list) =
  signals
  |> List.map (signalToDigit >> string)
  |> List.reduce (+)
  |> int

input
|> Seq.map (decode >> signalsToNumber)
|> Seq.sum
|> printfn "Part 2: %d"
