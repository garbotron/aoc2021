type Cell = { Value: int; Marked: bool }
type Board = Map<int * int, Cell>
type State = { Boards: Board list; ToCall: int list; Called: int }

let initialState =
  let str = System.IO.File.ReadAllText "input/day-04.txt"
  let chunks = str.Split "\n\n"
  let nums = chunks.[0].Split ',' |> Seq.toList |> List.map int
  let parseBoard (str: string) =
    let arr =
      str.Split '\n'
      |> Array.map (fun line -> line.Split(' ', System.StringSplitOptions.RemoveEmptyEntries) |> Array.map int)
    Seq.allPairs { 0 .. 4 } { 0 .. 4 }
      |> Seq.fold (fun map (x, y) -> map |> Map.add (x, y) { Value = arr.[y].[x]; Marked = false }) Map.empty
  let boards =
    chunks
    |> Seq.skip 1
    |> Seq.toList
    |> List.map parseBoard
  { Boards = boards; ToCall = nums; Called = -1 }

let turn state =
  let num = state.ToCall |> List.head
  let state = { state with ToCall = state.ToCall |> List.tail; Called = num }
  let mark board = board |> Map.map (fun _ cell -> { cell with Marked = cell.Marked || cell.Value = num })
  { state with Boards = state.Boards |> List.map mark }

let isWinner board =
  let isRowWin r = { 0 .. 4 } |> Seq.forall (fun c -> (board |> Map.find (c, r)).Marked)
  let isColWin c = { 0 .. 4 } |> Seq.forall (fun r -> (board |> Map.find (c, r)).Marked)
  { 0.. 4 } |> Seq.exists (fun i -> isRowWin i || isColWin i)

let score state board =
  let sum =
    board
    |> Map.filter (fun _ x -> not x.Marked)
    |> Map.toSeq
    |> Seq.map snd
    |> Seq.sumBy (fun x -> x.Value)
  sum * state.Called

let rec firstWinningScore state =
  match state.Boards |> List.tryFind isWinner with
  | Some x -> score state x
  | None -> state |> turn |> firstWinningScore

let rec lastWinningScore state =
  let state = state |> turn
  match state.Boards with
  | [x] -> score state x
  | boards ->
    { state with Boards = boards |> List.filter (not << isWinner) }
    |> lastWinningScore

printfn "Part 1: %d" (firstWinningScore initialState)
printfn "Part 2: %d" (lastWinningScore initialState)
